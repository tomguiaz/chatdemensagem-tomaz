var chatHistory = document.querySelector(".chat-history");
var input = document.querySelector("#message-text");
var list_messages = [];

(function(){
    list_messages = JSON.parse(localStorage.getItem('saved_messages'));
    if(list_messages === null){
        list_messages = [];
        
    }else{
        updateScreen();
    }
    
})();

function save(){
    //salva no local storage o estado atual do array
    localStorage.setItem('saved_messages',JSON.stringify(list_messages));
}
function removeFromArray(array, value){
    return array.filter(function(element){
        return element != value;
    })
}

function replaceElement(array, target, replacement){
    array.forEach(function(element, index){
        if(element === target){
            array[index] = replacement;
        }
    })
}
function saveEditing(event){
    let newTextValue = event.target.value;
    
    if(event.target.value.length>0 && (event.keyCode == 13)){
        messageContent = event.target.parentNode;
        let novaMensagem = document.createElement("p");
        novaMensagem.innerText=newTextValue;
        event.target.remove();
        messageContent.append(novaMensagem);
        event.target.editButton.style.visibility = 'visible'
        event.target.removeButton.style.visibility = 'visible'
        replaceElement(list_messages, event.target.oldText, newTextValue);
        save();
        console.log(list_messages);

    }
    
}
function createMessage(str){  
    //Cria a caixa de mensagem que contem o conteudo e os botões
    let message = document.createElement("div");
    message.classList.add("message");
    let index = list_messages.length+1;
    message.setAttribute('id', index);
    //Cria a div que abrigará o texto da mensagem, o que será uma tag p. Interna a message
    let messageContent = document.createElement("div");
    messageContent.classList.add("msg-content");

    //Cria o texto da mensagem, interno a messageContent
    let text = document.createElement("p");
    text.innerText = str;

    //Cria a div que abrigará os botões, interna a message
    let messageButtons = document.createElement("div");
    messageButtons.classList.add("msg-buttons");

    //Cria os botões de editar e excluir
    let btnEdit = document.createElement("button");
    let btnRemove = document.createElement("button");

    btnEdit.setAttribute('id', 'edit-btn');
    btnRemove.setAttribute('id', 'remove-btn');

    btnEdit.classList.add("btn");
    btnEdit.classList.add("msg-btn");

    btnRemove.classList.add("btn");
    btnRemove.classList.add("msg-btn");

    btnEdit.innerText="Editar";
    btnRemove.innerText="Excluir";

    //EDITA MENSAGEM
    btnEdit.onclick= function(){
        /*Textarea para edição*/ 
        let newText = document.createElement("textarea");
        let oldText = message.firstChild.firstChild.innerText;
        newText.value=oldText;
        newText.classList.add("msg-box-edit");
        var lines = newText.value.split('\n').length;
        newText.rows = lines;
        
        /*Remove paragrafo anterior */
        message.firstChild.firstChild.remove();
        /*Insere a textarea */
        message.firstChild.append(newText);
        
        /* Esconde os botões edit e remove*/
        btnEdit.style.visibility = 'hidden';
        btnRemove.style.visibility = 'hidden';
        
        newText.select();
        newText.oldText=oldText;
        
        newText.editButton = btnEdit;
        newText.removeButton = btnRemove;
        newText.addEventListener('keypress', saveEditing);
        console.log(newText);

    }

    //REMOVE MENSAGEM
    btnRemove.onclick = function(){
        //Cria uma nova lista, mas SEM o elemento que acabou de ser removido, na prática, também remove da lista o elemento deletado para que não haja conflito do visual com o que está armazenado. A remoção é através do conteudo da mensagem, message->msg-content->p->innertext
        list_messages = removeFromArray(list_messages, message.firstChild.firstChild.innerText);    

        save();
        console.log(list_messages);
        //faz a remoção visual da div
        message.remove();
        
    }
    //faz o append dos botões para o msg-buttons
    messageButtons.append(btnEdit);
    messageButtons.append(btnRemove);

    //faz o append do texto para o msg-content
    messageContent.append(text);

    //faz o append do msg-buttons e msg-content para o message 
    message.append(messageContent);
    message.append(messageButtons);

    
    chatHistory.append(message);
}

function showMessages(){
    list_messages.forEach((str)=>createMessage(str));
}
function updateScreen(){
    chatHistory.innerHTML="";
    showMessages();
}
function sendMessage(){

    //faz o append de message para o history
    list_messages.push(input.value);
    save();
    console.log(list_messages);
    
   // list_messages.push(createMessage());
 
    updateScreen();
    
    //Limpa a caixa de texto
    input.value = "";
}


function removeMessage(){
    let messageButtons = this.parentNode;
    let message = messageButtons.parentNode;
    console.log(message.getAttribute('id'));
}
let btn_send = document.querySelector("#btn-send");
btn_send.addEventListener("click", sendMessage);

